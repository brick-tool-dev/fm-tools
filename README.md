# Collection of Information about Formal-Methods Tools

## Motivation

There are many tools available that implement formal-methods approaches.
This repository collects meta data about the tools, such that
it becomes easier to reuse, integrate, and cooperate with formal-methods tools.

