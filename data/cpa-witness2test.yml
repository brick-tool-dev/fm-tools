name: CPA-witness2test
input_languages:
  - C
project_url: https://cpachecker.sosy-lab.org
repository_url: https://gitlab.com/sosy-lab/software/cpachecker
spdx_license_identifier: Apache-2.0
benchexec_toolinfo_module: benchexec.tools.cpa-witness2test
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - dbeyer
  - ricffb
  - lemberger

maintainers:
  - name: Dirk Beyer
    institution: LMU Munich
    country: Germany
    url: https://www.sosy-lab.org/people/dbeyer/
  - name: Philipp Wendler
    institution: LMU Munich
    country: Germany
    url: https://www.sosy-lab.org/people/pwendler/

versions:
  - version: "svcomp24-validation-violation"
    doi: 10.5281/zenodo.7700944
    benchexec_toolinfo_options: ['-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'analysis.summaryEdges=true', '-setprop', 'cpa.callstack.skipVoidRecursion=true', '-setprop', 'cpa.callstack.skipFunctionPointerRecursion=true']
    required_ubuntu_packages:
      - openjdk-11-jre-headless
  - version: "svcomp23-validation-violation"
    doi: 10.5281/zenodo.7700944
    benchexec_toolinfo_options: ['-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'analysis.summaryEdges=true', '-setprop', 'cpa.callstack.skipVoidRecursion=true', '-setprop', 'cpa.callstack.skipFunctionPointerRecursion=true']
    required_ubuntu_packages:
      - openjdk-11-jre-headless
  - version: "svcomp22-validation-violation"
    url: https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip
    benchexec_toolinfo_options: ['-setprop', 'witness.checkProgramHash=false', '-heap', '5000m', '-benchmark', '-setprop', 'analysis.summaryEdges=true', '-setprop', 'cpa.callstack.skipVoidRecursion=true', '-setprop', 'cpa.callstack.skipFunctionPointerRecursion=true']
    required_ubuntu_packages:
      - openjdk-11-jre-headless

competition_participations:
  - competition: "SV-COMP 2024"
    track: "Validation of Violation Witnesses 1.0"
    tool_version: "svcomp24-validation-violation"
    jury_member:
      name: Thomas Lemberger
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/lemberger/
  - competition: SV-COMP 2023
    track: "Validation of Violation Witnesses 1.0"
    tool_version: "svcomp24-validation-violation"
    jury_member:
      name: Henrik Wachowitz
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/wachowitz/

techniques:
  - Property-Directed Reachability
  - ARG-Based Analysis
  - Automata-Based Analysis

literature:
  - doi: 10.1007/978-3-319-92994-1_1
    title: "Tests from Witnesses: Execution-Based Validation of Verification Results"
    year: 2018
